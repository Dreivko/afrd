import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookService } from '../book.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  model = new Book('');

  constructor(private bookService: BookService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.bookService
      .save(this.model)
      /* TODO .subscribe(response => this.router.navigate(['']));*/
      .subscribe(response => console.log(response));
  }

}
