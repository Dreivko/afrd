import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BookFormComponent} from './book-form.component';
import {DebugElement} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {BookService} from '../book.service';
import {MockBookService} from '../../testing/mock-book.service';

import { RouterTestingModule } from '@angular/router/testing';

describe('BookFormComponent', () => {
  let component: BookFormComponent;
  let fixture: ComponentFixture<BookFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookFormComponent],
      imports: [
        FormsModule,
        RouterTestingModule
      ]
    })
      .overrideComponent(BookFormComponent, {
        set: {
          providers: [{provide: BookService, useClass: MockBookService}]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
